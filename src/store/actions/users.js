import * as COMMON from "../constants/common";
import * as USERS from "../constants/users";
import UserApi from "../../services/api-worker/UserApi";

export function getUsers({page, sort}) {
    return function (dispatch) {
        dispatch({
            type: COMMON.REQUEST_SENT
        });
        (new UserApi()).getUser(page, sort).then(res => {
            return dispatch({
                type: USERS.USERS_GET,
                data: res
            });
        });
    };
}

export function addUsers({page, sort}){
    return function (dispatch) {
        dispatch({
            type: COMMON.REQUEST_SENT
        });
        (new UserApi()).getUser(page, sort).then(res => {
            return dispatch({
                type: USERS.USERS_ADD,
                data: res,
            });
        })
    };
}
