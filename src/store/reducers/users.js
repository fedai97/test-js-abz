import * as COMMON from '../constants/common';
import * as USERS from '../constants/users';

const initState = {
	users: [],
	page: 0,
	totalPages: 0,
	loaded: false,
};

export default function (state = initState, action) {
	switch (action.type) {
		case COMMON.REQUEST_SENT:
			return {
				...state,
				...{
					loaded : false
				}
			};

		case USERS.USERS_GET:
			return {
				...state,
				...{
					users: action.data.users,
					page: action.data.page,
					totalPages: action.data.total_pages,
					loaded: true
				}
			};

		case USERS.USERS_ADD:
			return {
				...state,
				...{
					users: [...state.users, ...action.data.users],
					page: action.data.page,
					totalPages: action.data.total_pages,
					loaded : true,
				}
			};

		default:
			return state
	}
}
